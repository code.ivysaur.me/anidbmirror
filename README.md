# anidbmirror

![](https://img.shields.io/badge/written%20in-PHP-blue)

script to mirror data from AniDB.net to a local sqlite database.

Tags: anime, scraper

## Changelog

2015-11-01
- Initial public release


## Download

- [⬇️ anidb_mirror.php.gz](dist-archive/anidb_mirror.php.gz) *(1.95 KiB)*
